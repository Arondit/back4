<?php
header('Content-Type: text/html; charset=UTF-8');
$message = '';
$message_name = '';
$message_email = '';
$message_date = '';
$message_con = '';
$message_sex = '';
$message_power = '';
$message_bio = '';
$message_check = '';
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    $messages = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $message = 'УСПЕШНО';
    }

$errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['con'] = !empty($_COOKIE['con_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['power'] = !empty($_COOKIE['power_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);



if ($errors['name']) {
    setcookie('name_error', '', 100000);
    if($_COOKIE['name_error'] == '1') 
	{
      $message_name = '<div class="error"> Пустое поле у имени</div>';
    } 
}

if ($errors['email']) {
    setcookie('email_error', '', 100000);
    if($_COOKIE['email_error'] == '1') 
	{
      $message_email = '<div class="error"> Пустое поле в почте</div>';
    } 
	else 
	{
      $message_email = '<div class="error"> Неверные символы в почте</div>';
    }
  }


if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $message_date = '<div class="error"> Неверная дата </div>';
  }


if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $message_sex = '<div class="error"> Не выбран пол </div>';
  }


if ($errors['power']) {
    setcookie('power_error', '', 100000);
    $message_power = '<div class="error"> Не выбрана супер </div>';
  }


if ($errors['con']) {
    setcookie('con_error', '', 100000);
    $message_con = '<div class="error"> Не выбраны конечности </div>';
}

if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $message_bio = '<div class="error"> Пустая биография </div>';
}

if ($errors['check']) {
    setcookie('check_error', '', 100000);
    $message_check = '<div class="error"> СОГЛАСИТЕСЬ!!! </div>';
}

$values = array();
$values['name'] = (empty($_COOKIE['name_value']) || !preg_match('/^[а-яА-Я ]+$/u', $_COOKIE['name_value'])) ? '' : $_COOKIE['name_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['date'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['date_value'];
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['con'] = empty($_COOKIE['con_value']) ? '' : $_COOKIE['con_value'];
$values['power'] = empty($_COOKIE['power_value']) ? '' : $_COOKIE['power_value'];
$values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
$values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
include('cookie.php');
}

else {
    $errors = FALSE;
	
    if (empty($_POST['name'])) {
        setcookie('name_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('name_value', $_POST['name'], time() + 365 * 24 * 60 * 60);
    }


		
	if (empty($_POST['email']) || filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === FALSE ) {
		setcookie('email_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['date'])) {
		setcookie('date_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
	}

	if ( empty($_POST['sex'])) {
		setcookie('sex_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
	}
	

	if (empty($_POST['con'])) {
		setcookie('con_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('con_value', $_POST['con'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['power'])) {
		setcookie('power_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else 
	{
		$powers=serialize($_POST['power']);
		setcookie('power_value', $powers, time() + 365 * 24 * 60 * 60);
	}
	$immortal=in_array('immortal',$_POST['power']) ? '1' : '0';
	$wall=in_array('wall',$_POST['power']) ? '1' : '0';
	$levitation=in_array('levitation',$_POST['power']) ? '1' : '0';

	if (empty($_POST['bio'])) {
		setcookie('bio_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('bio_value', $_POST['bio'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['check'])) {
		setcookie('check_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		$check = $_POST['check'] ? '1' : '0';
		setcookie('check_value', $check, time() + 365 * 24 * 60 * 60);
	}
	$check = $_POST['check'] ? '1' : '0';

    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else {
        setcookie('name_error', '', 100000);
        setcookie('mail_error', '', 100000);
        setcookie('data_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('con_error', '', 100000);
        setcookie('power_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
    }

	$user = 'u17815';
    $pass = '9028103';
    $db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass);

 try
    {
        $stmt = $db->prepare("INSERT INTO application (name, email, date, sex, con, immortal, wall, levitation, bio) VALUES (:name, :email, :date, :sex, :con, :immortal, :wall, :levitation, :bio)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':sex', $sex);
        $stmt->bindParam(':con', $con);
        $stmt->bindParam(':immortal', $immortal);
		$stmt->bindParam(':wall', $wall);
		$stmt->bindParam(':levitation', $levitation);
        $stmt->bindParam(':bio', $bio);
        $stmt->execute();
        print ('Отправлено');
        exit();
    }
    catch(PDOException $e)
    {
        print ('Ошибка: ' . $e->getMessage());
        exit();
    }

setcookie('save', '1');

header('Location: index.php');
}
?>

